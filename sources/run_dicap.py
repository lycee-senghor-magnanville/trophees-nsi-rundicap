from random import*
import pygame
from pygame.locals import*
pygame.init()


                    #-----------------------------------------------------#
                    #             chargement des images                   #
                    #-----------------------------------------------------#

#ouverture de fenetre
fenetre=pygame.display.set_mode((1250,735))
pygame.display.set_caption("run'dicap")
#fond
fond=pygame.image.load('image/fondd.png').convert_alpha()
fond=pygame.transform.scale(fond,(15000,730))
#perso
perso=pygame.image.load('image/perso.gif').convert_alpha()
perso=pygame.transform.scale(perso,(100,100))
#arc
arc=pygame.image.load('image/arc.png').convert_alpha()
arc=pygame.transform.scale(arc,(100,100))
#tour
tour=pygame.image.load('image/tour.png').convert_alpha()
tour=pygame.transform.scale(tour,(100,100))
#bouteille
bouteille=pygame.image.load('image/bouteille.png').convert_alpha()
bouteille=pygame.transform.scale(bouteille,(100,100))
#haie
haie=pygame.image.load('image/haie.png').convert_alpha()
haie=pygame.transform.scale(haie,(100,100))
#basket
basket=pygame.image.load('image/basket.png').convert_alpha()
basket=pygame.transform.scale(basket,(100,100))
#foot
foot=pygame.image.load('image/foot.png').convert_alpha()
foot=pygame.transform.scale(foot,(100,100))
#rond
rond=pygame.image.load('image/rond.png').convert_alpha()
rond=pygame.transform.scale(rond,(100,100))
#tennis
tennis=pygame.image.load('image/tennis.png').convert_alpha()
tennis=pygame.transform.scale(tennis,(100,100))
#rugby
rugby=pygame.image.load('image/rugby.png').convert_alpha()
rugby=pygame.transform.scale(rugby,(100,100))
#coeur
coeur=pygame.image.load('image/coeur.png').convert_alpha()
coeur=pygame.transform.scale(coeur,(60,60))
coeur_casse=pygame.image.load('image/coeur_casse.png').convert_alpha()
coeur_casse=pygame.transform.scale(coeur_casse,(60,60))
#bling
bling=pygame.image.load('image/bling.png').convert_alpha()
bling=pygame.transform.scale(bling,(100,100))
#perdu
perdu=pygame.image.load('image/perdu.png').convert_alpha()
perdu=pygame.transform.scale(perdu,(1250,735))
#bravo
bravo=pygame.image.load('image/bravo.png').convert_alpha()
bravo=pygame.transform.scale(bravo,(1250,735))
#bouton replay
bouton=pygame.image.load('image/bouton.png').convert_alpha()
bouton=pygame.transform.scale(bouton,(100,100))
#fin
fin=pygame.image.load('image/fin.png').convert_alpha()
fin=pygame.transform.scale(fin,(200,200))
#plus
plus=pygame.image.load('image/plus.png').convert_alpha()
plus=pygame.transform.scale(plus,(200,200))
#regle
regle=pygame.image.load('image/regle.png').convert_alpha()
regle=pygame.transform.scale(regle,(1250,735))


                    #-----------------------------------------------------#
                    #                       menu                          #
                    #-----------------------------------------------------#

class Fenetre:
    def __init__(self,nom:str,image:str):
        self.nom=nom
        self.image = pygame.image.load(image).convert_alpha()
        self.image = pygame.transform.scale(self.image ,(1250,735))
        self.position =self.image.get_rect()
        self.position = self.position.move(0,0)

    def creation_fenetre(self):
        fenetre.blit(self.image,(0,0))
        pygame.display.flip()

    def attente_clic(self):
##        pygame.event.clear()
        pygame.display.flip()
        while True :
            for event in pygame.event.get() :
                if event.type == pygame.QUIT :
                    return False
                if event.type == KEYDOWN and event.key == K_ESCAPE:
                    return False
                if event.type == MOUSEBUTTONDOWN and event.button == 1 :
                    return event.pos[0],event.pos[1]



class jeu(Fenetre):

    def __init__(self,nom,image):
        Fenetre.__init__(self,nom,image)


    def musique(self,titre:str):#pour mettre de la musique
        pygame.mixer.music.load(titre)
        pygame.mixer.music.play() # lancer la lecture
        pygame.mixer.music.set_volume(0.5) # volume à 50 %
        pygame.mixer.music.play(-1) # musique jouer en boucle

    def boucle_jeu(self):
        x=0
        y=490
        z=1000
        d=2345
        d1=0
        d2=0
        d3=0
        d4=0
        d5=0
        d6=0
        d7=0
        d8=0
        pv=0
        vie=3
        compteur_arc=0
        compteur_tour=0
        compteur_bouteille=0
        compteur_basket=0
        compteur_foot=0
        compteur_rond=0
        compteur_tennis=0
        compteur_rugby=0
        fenetre.blit(fond,(x,0))
        fenetre.blit(fin,(x+13550,150))
        fenetre.blit(perso,(0,y))
        fenetre.blit(arc,(x+10300,490))
        fenetre.blit(tour,(x+8200,280))
        fenetre.blit(bouteille,(x+2000,595))
        fenetre.blit(basket,(x+1500,275))
        fenetre.blit(foot,(x+500,595))
        fenetre.blit(rond,(x+3000,385))
        fenetre.blit(tennis,(x+2500,490))
        fenetre.blit(rugby,(x+3500,490))
        fenetre.blit(coeur,(920,0))
        fenetre.blit(haie,(d1+1220,490))
        fenetre.blit(haie,(d2+1220+967,490))
        fenetre.blit(haie,(d3+1500,595))
        fenetre.blit(haie,(d4+1500+389,595))
        fenetre.blit(haie,(d5+1570,385))
        fenetre.blit(haie,(d6+1507+473,385))
        fenetre.blit(haie,(d7+1000,280))
        fenetre.blit(haie,(d8+1800,280))
        # rectangle pour collison
        perso_rect=pygame.Rect(0,y,100,100)
        arc_rect=pygame.Rect(x+10300,490,100,100)
        tour_rect=pygame.Rect(x+8200,280,100,100)
        bouteille_rect=pygame.Rect(x+2000,595,100,100)
        basket_rect=pygame.Rect(x+1500,275,100,100)
        foot_rect=pygame.Rect(x+500,595,100,100)
        rond_rect=pygame.Rect(x+3000,385,100,100)
        tennis_rect=pygame.Rect(x+2500,490,100,100)
        rugby_rect=pygame.Rect(x+3500,490,100,100)
        haie1_rect=pygame.Rect(d1+1220,490,100,100)
        haie2_rect=pygame.Rect(d2+1220+967,490,100,100)
        haie3_rect=pygame.Rect(d3+1500,595,100,100)
        haie5_rect=pygame.Rect(d5+1570,385,100,100)
        haie6_rect=pygame.Rect(d6+1507+473,385,100,100)
        haie7_rect=pygame.Rect(d7+1000,280,100,100)
        gagner=False
        perdue=False
        continuer=True
        while continuer:
            x-=2
            d1-=2
            d2-=2
            d3-=2
            d5-=2
            d6-=2
            d7-=2
            if d1<-1355:
                d1=randint(200,600)
                fenetre.blit(haie,(d1+1220,490))
                haie1_rect=pygame.Rect(d1+1220,490,100,100)
            if d2<-2316:
                d2=randint(200,600)
                fenetre.blit(haie,(d2+1220+967,490))
                haie2_rect=pygame.Rect(d2+1220+967,490,100,100)
            if d3<-1655:
                d3=randint(200,600)
                fenetre.blit(haie,(d3+1500,595))
                haie3_rect=pygame.Rect(d3+1500,595,100,100)
            if d5<-1655:
                d5=randint(200,600)
                fenetre.blit(haie,(d5+1570,385))
                haie5_rect=pygame.Rect(d5+1570,385,100,100)
            if d6<-2128:
                d6=randint(200,600)
                fenetre.blit(haie,(d6+1507+473,385))
                haie6_rect=pygame.Rect(d6+1507+473,385,100,100)
            if d7<-1255:
                d7=randint(200,600)
                fenetre.blit(haie,(d7+1000,280))
                haie7_rect=pygame.Rect(d7+1000,280,100,100)
            if x<=-13750:
                gagner=True
                continuer=False
            for event in pygame.event.get():
                if event.type==pygame.QUIT:
                    continuer=False
                if event.type==KEYDOWN:
                    if event.key==K_ESCAPE:
                        continuer=False
                    if event.key==K_DOWN:# changement de coordonnee (vers le bas)
                        if y<593:
                            y+=105
                    if event.key==K_UP:# changement de coordonee (vers le haut)
                        if y>380:
                            y-=105
            #re-collage
            fenetre.blit(fond,(x,0))
            fenetre.blit(perso,(0,y))
            fenetre.blit(fin,(x+13550,150))
            #obstacles
            fenetre.blit(haie,(d1+1220,490))
            fenetre.blit(haie,(d2+1220+967,490))
            fenetre.blit(haie,(d3+1500,595))
            fenetre.blit(haie,(d5+1570,385))
            fenetre.blit(haie,(d6+1507+473,385))
            fenetre.blit(haie,(d7+1000,280))
            # rectangle pour collison
            perso_rect=pygame.Rect(0,y,100,100)
            haie1_rect=pygame.Rect(d1+1220,490,100,100)
            haie2_rect=pygame.Rect(d2+1220+967,490,100,100)
            haie3_rect=pygame.Rect(d3+1500,595,100,100)
            haie5_rect=pygame.Rect(d5+1570,385,100,100)
            haie6_rect=pygame.Rect(d6+1507+473,385,100,100)
            haie7_rect=pygame.Rect(d7+1000,280,100,100)
            #collision avec les objets a recuperer
            if perso_rect.colliderect(arc_rect):
                compteur_arc=1
                fenetre.blit(bling,(x+10300,490))
            if compteur_arc==0:
                fenetre.blit(arc,(x+10300,490))
                arc_rect=pygame.Rect(x+10300,490,100,100)
            if perso_rect.colliderect(tour_rect):
                compteur_tour=1
                fenetre.blit(bling,(x+8200,280))
            if compteur_tour==0:
                fenetre.blit(tour,(x+8200,280))
                tour_rect=pygame.Rect(x+8200,280,100,100)
            if perso_rect.colliderect(basket_rect):
                compteur_basket=1
                fenetre.blit(bling,(x+1500,275))
            if compteur_basket==0:
                fenetre.blit(basket,(x+1500,275))
                basket_rect=pygame.Rect(x+1500,275,100,100)
            if perso_rect.colliderect(bouteille_rect):
                compteur_bouteille=1
                fenetre.blit(bling,(x+2000,595))
            if compteur_bouteille==0:
                fenetre.blit(bouteille,(x+2000,595))
                bouteille_rect=pygame.Rect(x+2000,595,100,100)
            if perso_rect.colliderect(foot_rect):
                compteur_foot=1
                fenetre.blit(bling,(x+5000,595))
            if compteur_foot==0:
                fenetre.blit(foot,(x+5000,595))
                foot_rect=pygame.Rect(x+5000,595,100,100)
            if perso_rect.colliderect(rond_rect):
                compteur_rond=1
                fenetre.blit(bling,(x+3000,385))
            if compteur_rond==0:
                fenetre.blit(rond,(x+3000,385))
                rond_rect=pygame.Rect(x+3000,385,100,100)
            if perso_rect.colliderect(tennis_rect):
                compteur_tennis=1
                fenetre.blit(bling,(x+2500,490))
            if compteur_tennis==0:
                fenetre.blit(tennis,(x+2500,490))
                tennis_rect=pygame.Rect(x+2500,490,100,100)
            if perso_rect.colliderect(rugby_rect):
                compteur_rugby=1
                fenetre.blit(bling,(x+3500,490))
            if compteur_rugby==0:
                fenetre.blit(rugby,(x+3500,490))
                rugby_rect=pygame.Rect(x+3500,490,100,100)
            somme=compteur_arc+compteur_basket+compteur_foot+compteur_rond+compteur_rugby+compteur_tennis+compteur_tour
            if compteur_bouteille==1:
                x-=2.5
                d1-=2.5
                d2-=2.5
                d3-=2.5
                d5-=2.5
                d6-=2.5
                d7-=2.5
            #collision avec les obstacles
            if perso_rect.colliderect(haie1_rect) or perso_rect.colliderect(haie2_rect) or perso_rect.colliderect(haie3_rect) or perso_rect.colliderect(haie5_rect) or perso_rect.colliderect(haie6_rect) or perso_rect.colliderect(haie7_rect): #collision entre personnage et obstacles
                pv-=1
                if -1>pv>-66:# si on se fait toucher notre coeur perd de la vie
                    vie=2
                if -67>pv>-133:
                    vie=1
                if -134>pv>-2000:
                    vie=0
            if vie==3:
                fenetre.blit(coeur,(920,0))
            if vie==2:
                fenetre.blit(coeur_casse,(920,0))


            if vie==0 : # on perd si on a plus de vie
                perdue=True
                continuer=False
                x=0
                fenetre.blit(perdu,(0,0))
                fenetre.blit(bouton,(340,360))#bouton replay
                (x_clic,y_clic)=menu.attente_clic()
                if 349 <x_clic<432 and 372<y_clic<445: #on recommence si on appuie sur le bouton
                    run.boucle_jeu()
            if x<=-13750 and somme<7:
                perdue=True
                continuer=False
                x=0
                fenetre.blit(perdu,(0,0))
                fenetre.blit(bouton,(340,360))#bouton replay
                (x_clic,y_clic)=menu.attente_clic()
                if 349 <x_clic<432 and 372<y_clic<445: #on recommence si on appuie sur le bouton
                    run.boucle_jeu()
            if somme==7 and x<=-13750:
                gagner=True
                continuer=False
                fenetre.blit(bravo,(0,0))
                fenetre.blit(bouton,(340,360))#bouton replay
                (x_clic,y_clic)=menu.attente_clic()
                if 349 <x_clic<432 and 372<y_clic<445:#on recommence si on appuie sur le bouton
                    run.boucle_jeu()
            pygame.display.flip()




menu=Fenetre("menu","image/menu_sport.png")
menu.creation_fenetre()
(x_clic,y_clic)=menu.attente_clic()
print(x_clic,y_clic)
if 1005<x_clic<1167 and 60<y_clic<180:#pour afficher les regles du jeu
    fenetre.blit(regle,(0,0))
    (x_clic,y_clic)=menu.attente_clic()
    pygame.display.flip()
    if 145<x_clic<403 and 641<y_clic<700:# le jeu commenece si on appuie sur le rectangle start
        run = jeu(Fenetre,"image/fond.png")
        run.musique("rundcap.wav")
        run.boucle_jeu()
        pygame.display.flip()
if 145<x_clic<403 and 641<y_clic<700:# le jeu commenece si on appuie sur le rectangle start
    run = jeu(Fenetre,"image/fond.png")
    run.musique("rundcap.wav")
    run.boucle_jeu()
    pygame.display.flip()